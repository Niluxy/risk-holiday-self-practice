package SelfPractise27;

public class ConceptCheck {
	public static void main(String args[]) {
		String s=("Mississippi");
		
		System.out.println("a) s.length(): "+s.length());
		System.out.println("b) s.indexOf(\"si\"): "+s.indexOf("si"));
		System.out.println("c) s.toUpperCase().indexOf(\"si\"): "+s.toUpperCase().indexOf("si"));
		System.out.println("d) s.toLowerCase().indexOf(\"si\"): "+s.toLowerCase().indexOf("si"));
		System.out.println("e) s.substring(0,s.indexOf(\"i\")): "+s.substring(0,s.indexOf("i")));
		System.out.println("f) s.substring(s.lastIndexOf(\"i\")): "+s.substring(s.lastIndexOf("i")));


	}

}
/*
a) s.length(): 11
b) s.indexOf("si"): 3
c) s.toUpperCase().indexOf("si"): -1
d) s.toLowerCase().indexOf("si"): 3
e) s.substring(0,s.indexOf("i")): M
f) s.substring(s.lastIndexOf("i")): i

/*