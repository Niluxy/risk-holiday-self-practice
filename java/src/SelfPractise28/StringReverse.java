package SelfPractise28;

import java.util.Scanner;

public class StringReverse {
	public static void main(String args[]) {
		String initial,reverse ="";
		Scanner scan=new Scanner(System.in);
		System.out.print("Enter an input String:");
		
		initial=scan.nextLine();
		
		int length=initial.length();
		
		for(int i=length-1;i>=0;i--)
		reverse=reverse+( initial.charAt(i));
		
		
		System.out.println("The reverse of Computer-Programming is" +" "+reverse);
		
		
	}

}

/*
output

Enter an input String:Computer-Programming
The reverse of Computer-Programming is gnimmargorP-retupmoC

/*