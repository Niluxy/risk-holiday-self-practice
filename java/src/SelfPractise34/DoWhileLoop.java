package SelfPractise34;

public class DoWhileLoop {
	public static void main(String args[]) {	
		int num = 1;
		int num1 = 10;
		
		do {
			int oddNum = ((num * 2) - 1);
			int eveeNum = num * 2;
			String oddResult = String.format("%03d", oddNum);
			String evenResult = String.format("%03d", evenNum);
			System.out.println(oddResult + ".0" + "," + evenResult + ".0");
			num = num + 1;
		}
			while (num <= num1 / 2);
		
		 }
	}


/*output
001.0,002.0
003.0,004.0
005.0,006.0
007.0,008.0
009.0,010.0

 */


