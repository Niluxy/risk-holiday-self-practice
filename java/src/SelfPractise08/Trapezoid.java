package SelfPractise08;

public class Trapezoid {
	public static void main (String args[]) {
		double top=10.0;
		double bottom=20.5;
		double height=24.4;
		double area=(bottom+top)*height/2;
		
		System.out.print("top \t:");
		System.out.println(top);
		System.out.print("bottom \t:");
		System.out.println(bottom);
		System.out.print("height \t:");
		System.out.println(height);
		System.out.print("area \t:");
		System.out.println(area);
	}
}

/*
output
top 	:10.0
bottom 	:20.5
height 	:24.4
area 	:372.09999999999997
/*