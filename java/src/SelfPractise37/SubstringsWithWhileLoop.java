package SelfPractise37;

public class SubstringsWithWhileLoop {
	
public static void main(String args[]) {
	String word=(" sebastian-ibis");
	int str=word.length();
	int j=str-1;
	
	while(j>0) {
		System.out.println(word.substring(j));
		j--;
	}
			
  }
	
}

/*
  
s
is
bis
ibis
-ibis
n-ibis
an-ibis
ian-ibis
tian-ibis
stian-ibis
astian-ibis
bastian-ibis
ebastian-ibis
sebastian-ibis

/*
