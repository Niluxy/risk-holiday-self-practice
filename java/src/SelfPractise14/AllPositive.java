package SelfPractise14;

import java.util.Scanner;

public class AllPositive {
	public static void main(String args[]) {
		Scanner input=new Scanner (System.in);
		System.out.print("Value A:");
		double valueA=input.nextDouble();
		System.out.print("Value B:");
		double valueB=input.nextDouble();
		System.out.print("Value C:");
		double valueC=input.nextDouble();
		
		System.out.print("All are positive:"+positive(valueA,valueB,valueC));
	}
		public static Boolean positive(double valueA,double valueB,double valueC) {
			return
					(valueA>=0)&&(valueB>=0)&&(valueC>=0);
			
					
			
		}
		
	}


/*
output
Value A:6
Value B:8
Value C:9
All are positive:true
/*