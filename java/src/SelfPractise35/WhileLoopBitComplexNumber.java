package SelfPractise35;

public class WhileLoopBitComplexNumber{
	public static void main(String args[]) {
		int i = 10;
		
		while (i>=0){
			int result = (int) Math.pow(2,i);
			String formatstr = String.format("%04d", result);
			System.out.println("+" + formatstr + "?");
			i--;
			}
	}
}

/*
+1024?
+0512?
+0256?
+0128?
+0064?
+0032?
+0016?
+0008?
+0004?
+0002?
+0001?

/*