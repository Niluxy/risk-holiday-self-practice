package SelfPractise21;

import java.util.Scanner;

public class ReceiveAndPrint {
	public static void main(String args[]) {
		Scanner scan=new Scanner(System.in);
		System.out.print("Enter String value:");
		String s=scan.nextLine();
		System.out.print("Enter Integer value:");
		int m=scan.nextInt();
		System.out.print("Enter Double value:");
		double d=scan.nextDouble();
		
		System.out.println("s:"+s);
		System.out.println("m:"+m);
		System.out.println("d:"+d);
		System.out.println("m + d + s:"+( m + d + s ));
		System.out.println("m + d + s:"+(m + d + s));
		System.out.println("s + m + d:"+(s + m + d));
	}

}

/*
output
Enter String value:Niluxy
Enter Integer value:09
Enter Double value:3.6
s:Niluxy
m:9
d:3.6
m + d + s:12.6Niluxy
m + d + s:12.6Niluxy
s + m + d:Niluxy93.6

/*