package SelfPractise23;

public class StringMethod {
	public static void main(String args[]) {
		String words= "School.of.Progressive. Rock";
		
		System.out.println("A.word.length():"+words.length());
		System.out.println("B.word.substring(22):"+words.substring(22));
		System.out.println("C.word.substring(22,24):"+words.substring(22,24));
		System.out.println("D.word.indexOf(\"oo\"):"+words.indexOf("oo"));
		System.out.println("E.word.toUpperCase():"+words.toUpperCase());
		System.out.println("F.word.lastIndexOf(\"o\"):"+words.lastIndexOf("o"));
		System.out.println("G.word.indexOf(\"ok\"):"+words.indexOf("ok"));
		}
	}

/*
A.word.length():27
B.word.substring(22): Rock
C.word.substring(22,24): R
D.word.indexOf("oo"):3
E.word.toUpperCase():SCHOOL.OF.PROGRESSIVE. ROCK
F.word.lastIndexOf("o"):24
G.word.indexOf("ok"):-1

/*