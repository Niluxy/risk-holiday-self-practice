package SelfPractise13;

import java.util.Scanner;

public class IsRightAngleTriangle {
	public static void main(String args[]) {
	Scanner scan =new Scanner(System.in);
	System.out.print("Side 1:");
	double side1=scan.nextDouble();
	System.out.print("Side 2:");
	double side2=scan.nextDouble();
	System.out.print("Side 3:");
	double side3=scan.nextDouble();
	
	System.out.print("IsRightAngleTriangle : " + RightAngleTriangle(side1,side2,side3));
}

	public static boolean RightAngleTriangle(double side1,double side2,double side3) {
		if(Math.pow(side1, 2)+Math.pow(side2, 2) == Math.pow(side3, 2))
		return true;
		if(Math.pow(side1, 2)+Math.pow(side3, 2) == Math.pow(side2, 2))
		return true;
		if(Math.pow(side2, 2)+Math.pow(side3, 2) == Math.pow(side1, 2))
		return true;
		else 
		return false;
	}
}
		
	
	
/*
 * output	
 */
Side 1:5
Side 2:4
Side 3:3
IsRightAngleTriangle : true
	



