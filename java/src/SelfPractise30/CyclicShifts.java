package SelfPractise30;

import java.util.Scanner;


public class CyclicShifts {
	
		public static  String print(String text, int shift) {

				if (shift == 0 || shift == text.length()) {
					return text;
				}
				String prefix = text.substring(0, shift);
				String postfix = text.substring(shift);
				return postfix + prefix;
			}

			public static void main(String args[]) {
				Scanner scan=new Scanner(System.in);
				System.out.print("Enter the input value : ");
				String word = scan.nextLine();
				System.out.print("Enter the shift value k : ");
				int k = scan.nextInt();
				
				if((k>=0) && (k<= word.length())) {

					switch (k) {
					case 1:
						System.out.println("\nThe 1-st cyclic shift of");
						break;
					case 2:
						System.out.println("\nThe 2-nd cyclic shift of");
						break;
					case 3:
						System.out.println("\nThe 3-rd cyclic shift of");
						break;
					
					default:
						System.out.println("\nThe " + k + "-th cyclic shift of");
					}

					System.out.println("\"" + word + "\"\nis");
					System.out.println("\"" + print(word, k) + "\"");

				} else {
					System.out.println("\nvalue is invalid and stop");
				}

			}
		}

			
/*
Enter the input value : how are you?
Enter the shift value k : 4

The 4-th cyclic shift of
"how are you?"
is
"are you?how "
/*
		
		
			
		