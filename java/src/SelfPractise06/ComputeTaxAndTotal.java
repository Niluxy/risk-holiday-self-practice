package SelfPractise06;

import java.util.Scanner;

public class ComputeTaxAndTotal {
	public static void main(String args[]) {
	Scanner input=new Scanner(System.in);
	System.out.print("Enter SubTotal:");
		int subTotal=input.nextInt();
		System.out.print("Enter TaxPercent:");
		double taxPercent=input.nextDouble();
		
		int tax=(int)(subTotal*taxPercent/100);
		int total=(subTotal+tax);
		
		System.out.println("The subtotal = "+subTotal/100+" dollars and "+subTotal%100+" cents.");
		System.out.println("The tax rate = "+ taxPercent +" percent.");
		System.out.println("The tax = "+tax/100+" dollars and "+tax%100+" cents.");
		System.out.println("The total = "+ total/100+" dollars and "+ total%100+" cents.");
		}
	
}

/*
output
Enter SubTotal:890
Enter TaxPercent:8
The subtotal = 8 dollars and 90 cents.
The tax rate = 8.0 percent.
The tax = 0 dollars and 71 cents.
The total = 9 dollars and 61 cents.
/*