package SelfPractise09;

import java.util.Scanner;

public class PlayWithNumberDecompose {
	public void test1(int a1,int a2) {
		Scanner scan=new Scanner(System.in);
		System.out.print("Enter two integers:");
		a1=scan.nextInt();
		a2=scan.nextInt();
		
		System.out.println("a+b is Equal to:"+(a1+a2));
		System.out.println("a-b is Equal to:"+(a1-a2));
		System.out.println("a*b is Equal to:"+(a1*a2));
		System.out.println("a/b is Equal to:"+(a1/a2));
		System.out.println("a%b is Equal to:"+(a1%a2));
	
		System.out.println(" ");
	}
		public void test2(int a1,int a2,int a3) {
			Scanner scan=new Scanner(System.in);
			System.out.print("Enter three integers:");
			a1=scan.nextInt();
			a2=scan.nextInt();
			a3=scan.nextInt();
		
			System.out.println("a- b)/c is equal to:"+(a1- a2)/a3);
			System.out.println("a- c)/b is equal to:"+(a1- a3)/a2);
			System.out.println("b- a)/c is equal to:"+(a2- a1)/a3);
			System.out.println("b- c)/a is equal to:"+(a2- a3)/a1);
			System.out.println("c- a)/b is equal to:"+(a3- a1)/a2);
			System.out.println("c- b)/a is equal to:"+(a3- a2)/a1);
		}
	
public static void main(String args[]) {
	PlayWithNumberDecompose demo= new PlayWithNumberDecompose();
	int a1=0;
	int a2=0;
	int a3=0;
	demo.test1(a1,a2);
	demo.test2(a1,a2,a3);
	}
}
/*
output

Enter two integers:450 340
a+b is Equal to:790
a-b is Equal to:110
a*b is Equal to:153000
a/b is Equal to:1
a%b is Equal to:110
 
Enter three integers:2300 560 450
a- b)/c is equal to:3
a- c)/b is equal to:3
b- a)/c is equal to:-3
b- c)/a is equal to:0
c- a)/b is equal to:-3
c- b)/a is equal to:0

/*
